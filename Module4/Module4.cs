﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
           
        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return array.Max();
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return array.Min();
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            int sum = 0;
            foreach (var item in array)
            {
                sum += item;
            }
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            return array.Max() - array.Min();
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            int max = array.Max();
            int min = array.Min();
            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += max;
                }
                else
                {
                    array[i] -= min;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] sum;
            if (a.Length >= b.Length)
            {
                sum = new int[a.Length];
                for (int i = 0; i < b.Length; i++)
                {
                    sum[i] = a[i] + b[i];
                }

                for (int i = b.Length; i < a.Length; i++)
                {
                    sum[i] = a[i];
                }
            }
            else
            {
                sum = new int[b.Length];
                for (int i = 0; i < a.Length; i++)
                {
                    sum[i] = a[i] + b[i];
                }
                for (int i = a.Length; i < b.Length; i++)
                {
                    sum[i] = b[i];
                }
            }
                
            return sum;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
                throw new ArgumentException("Radius is negative");
            length = 2 * radius * Math.PI;
            square = Math.PI * radius * radius;
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            sumOfItems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sumOfItems += array[i];
            }
            Array.Sort(array);
            minItem = array[0];
            Array.Reverse(array);
            maxItem = array[0];
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;

            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
                throw new ArgumentException("array");
            double length = 2 * radius * Math.PI;
            double square = Math.PI * radius * radius;

            return (length, square);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            int sumOfItems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sumOfItems += array[i];
            }
            Array.Sort(array);
            int minItem = array[0];
            Array.Reverse(array);
            int maxItem = array[0];

            return (minItem, maxItem, sumOfItems);
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array");
            if (direction == SortDirection.Ascending)
            {
                Array.Sort(array);
            }
            else
            {
                Array.Sort(array);
                Array.Reverse(array);
            }
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            throw new NotImplementedException();
        }
    }
}
